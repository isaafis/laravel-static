<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form>
        <!-- name -->
        <p>First name:</p>
        <input type="text" id="fname" name="fname"><br>
        <p>Last name:</p>
        <input type="text" id="lname" name="lname"><br>
        <!-- gender -->
        <p>Gender :</p>
        <input type="radio" id="html" name="fav_language" value="HTML">
        <label for="html">Male</label><br>
        <input type="radio" id="css" name="fav_language" value="CSS">
        <label for="css">Female</label><br>
        <input type="radio" id="javascript" name="fav_language" value="JavaScript">
        <label for="javascript">Other</label>
      </form>
      <!-- negara -->
      <p>Nationality :</p>
    <select id="language" name="language">
        <option value="Indonesia">Indonesia </option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Malaysia">Australia</option>
    </select>
    <!-- catatan -->
    <p>Bio :</p>
    <textarea name="message" rows="10" cols="30"></textarea><br>
    <a href="/welcome">
        <input type="submit" >
    </a>
</body>
</html>
{{-- 
$table->bigIncrements('id');
$table->text('content');
$table->integer('point');
$table->unsignedBigInteger('user_id');
$table->foreign('user_id')->references('id')->on('users');
$table->unsignedBigInteger('film_id');
$table->foreign('film_id')->references('id')->on('film');
$table->timestamps(); --}}